from django.db import *
from django.contrib.auth.models import User
from tabnanny import verbose
from datetime import date
from django.db import models
from django import forms
from django.contrib.auth.forms import UserCreationForm
from tkinter import CASCADE

class puesto(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_puesto = models.CharField(max_length=10, blank=False, null=False, unique=True)
    fecha_creacion = models.DateField(blank=False, null=False,auto_now_add=True)
    fecha_modificacion = models.DateField(blank=False, null=False, auto_now=True)
    estado = models.BooleanField(blank=False, null=False, default=True)

    class Meta:
        verbose_name = 'Puesto'  
        verbose_name_plural = 'Puestos'

    def __str__(self):
        return self.nombre_puesto

class reserva(models.Model):
    id = models.AutoField(primary_key=True)
    puesto = models.ForeignKey(puesto, on_delete=models.CASCADE,null=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_reserva = models.DateField(blank=False, null=False, unique=False)

    class Meta:
        verbose_name = 'reserva'
        verbose_name_plural = 'reservas'
        unique_together = ['puesto', 'fecha_reserva']

    def __str__(self):
        return self.usuario.username












