from urllib import response
from .models import *
from multiprocessing import context
from django.shortcuts import render, redirect
from rest_framework.views import APIView
from rest_framework import viewsets
from django.http import HttpResponse, JsonResponse
from .serializers import PuestoSerializer, ReservaSerializer, SeatReservationRequestSerializer
from rest_framework.decorators import api_view
from rest_framework import status
from django.shortcuts import render,redirect,get_object_or_404
from . forms import UserRegisterForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required   
from django.contrib.auth.models import User
from datetime import date
from rest_framework.response import Response
from json import loads
from django.db.models import Q
from datetime import datetime


# Create your views here.

def validacion_usuario(request):
    print(request.user)
    if request.user:
        return redirect('/')
        
def register(request):
    validacion_usuario(request)

    if request.method == "GET":
        form = UserRegisterForm()
        context={'form' : form}
        return render(request,'registration/register.html',context) 
    
    if request.method=='POST' :
        form = UserRegisterForm(request.POST)


        if form.is_valid():
            form.save()
            username= form.cleaned_data['username']
            messages.success(request, f' usuario {username} creado')
            return redirect('home')
            # return self.form_invalid(form)

        else:
            form_to_json = loads(form.errors.as_json(escape_html=True))
        
            for field_name, message_list in form_to_json.items():
                for message in message_list:
                    messages.error(request, message.get("message", ""))

            context={'form' : form}
            return render(request,'registration/register.html',context)


def login(request):
    # return render(request,'registration/login.html')
    response = redirect('/')
    return response

# Create your views here.
@login_required
def home(request):
    return render(request,'vistas/mis_reservas.html')


@api_view(['GET'])
def puestos_disponibles(request):
    if request.query_params:
        params = request.GET['fecha_a_reservar']
        puest = puesto.objects.filter(~Q(reserva__fecha_reserva=params))
    else:
        puest=puesto.objects.all()
    response=PuestoSerializer(puest,many=True)
    
    return Response(response.data)


@login_required
# Vista del inicio hacer reserva
def inicio(request):
    usuario = User.objects.get(username=request.user)
    reservas_usuario = reserva.objects.filter(usuario=usuario)

    context={
        'usuario':request.user,
        'mis_reservas':reservas_usuario
    }

    if request.method == "POST":

        try:
            puesto_a_reservar = request.POST.get("puesto")
            eleccion = puesto.objects.get(id=puesto_a_reservar)        
            usuario = request.user
            username = User.objects.get(username=usuario)
            fecha_reserva = request.POST["fecha_reserva"] 
            
            fecha_actual = str(datetime.now())

            if fecha_reserva >= fecha_actual:
                reserva(puesto=eleccion, usuario=username, fecha_reserva=fecha_reserva).save()
                messages.success(request,'Reserva realizada')
            else:
                messages.error(request,'Fecha de la reserva no es correcta')
        except:
            messages.error(request,'Ingresa los datos de la reserva')

    return render( request, 'vistas/mis_reservas.html',context)

# #Vista de ver mis reservas

def ver_reserva(request): 
    hoy = date.today()
    usuario = User.objects.get(username=request.user)
    reservas_usuario = reserva.objects.filter(fecha_reserva__gte=hoy,usuario=usuario)
    print(ver_reserva)
    context={
        'usuario':request.user,
        'reserva_usuario':reservas_usuario
    }
   
    return render( request, 'vistas/mis_reservas.html',context)

def eliminar_reserva(request,id):
    Nueva_reserva=get_object_or_404(reserva, id = id)
    Nueva_reserva.delete()
    messages.success(request,'Se elimino la reserva')
    return redirect(to='home')

#api con funciones- del modelo puesto (get,post)
@api_view(['GET', 'POST'])
def obtener_sillas(request):
    fecha = request.GET.get('fecha')
    if fecha:
        reservas = reserva.objects.filter(fecha_reserva=fecha)
        puestos_reservados = [res.puesto_id for res in reservas]
        sillas = puesto.objects.all().exclude(id__in=puestos_reservados)
    else:
        sillas = puesto.objects.all()
    response = PuestoSerializer(sillas, many=True)
    return Response(response.data)

#api con funciones- del modelo puesto (put,delete)
@api_view(['GET', 'PUT', 'DELETE'])
def putdelete_sillas(request, pk):   
    try:
        sillas = puesto.objects.get(pk=pk)
    except puesto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
#list
    if request.method == 'GET':
        serializer = PuestoSerializer(sillas)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PuestoSerializer(sillas, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        sillas.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#api con funciones- del modelo reserva (get,post)

@api_view(['GET', 'POST'])
def obtener_reservas(request):
   
    if request.method == 'GET':
        reservas = reserva.objects.all()
        response = ReservaSerializer(reservas,many=True)
        return Response(response.data)


    elif request.method == 'POST':

        print(f"keys: {request.data.keys()}")
        serializer = SeatReservationRequestSerializer(request.data)
        print(f"keys: {serializer.data.keys()}")

        if request.data.keys() != serializer.data.keys():
            print("DIFERENTES!!\n")
            return Response("Error fields", status= status.HTTP_400_BAD_REQUEST)
        seat_name = serializer.data['puesto']
        email = serializer.data['email']
        
        queryset_result= puesto.objects.values_list('estado', flat=True).get(nombre_puesto=seat_name)
        print(f'queryset: {queryset_result}')

        if queryset_result:
            puesto.objects.filter(nombre_puesto=seat_name).update(estado=False)
            puesto.objects.filter(nombre_puesto=seat_name).update(email=email)
            return Response("Silla reservada exitosamente.")
        else:
            return Response("Esa silla ha sido reservada previamente.")

#api con funciones- del modelo reserva (delete)

@api_view(['GET', 'PUT', 'DELETE'])
def putdelete_reservas(request, pk):
   
    try:
        reservas = reserva.objects.get(pk=pk)
    except reserva.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ReservaSerializer(reservas)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PuestoSerializer(reservas, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        reservas.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#api con clases- del modelo puesto
class puesto_Viewsets(viewsets.ModelViewSet):
    queryset = puesto.objects.all()
    serializer_class = PuestoSerializer

class reserva_Viewsets(viewsets.ModelViewSet):
    queryset = reserva.objects.all()
    serializer_class = ReservaSerializer

