from django.apps import AppConfig


class MisReservasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mis_reservas'
