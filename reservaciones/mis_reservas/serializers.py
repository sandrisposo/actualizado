from django.contrib.auth.models import User
from rest_framework import serializers 
from rest_framework import permissions
from mis_reservas.models import reserva, puesto


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username']
      
class PuestoSerializer(serializers.ModelSerializer):    
     class Meta:
        model = puesto
        exclude = ['fecha_creacion','fecha_modificacion']


class ReservaSerializer(serializers.ModelSerializer):
    puesto = PuestoSerializer (many=False, read_only=True)
    class Meta:
        model = reserva
        fields = ['puesto','id', 'usuario', 'fecha_reserva']
        
class SeatReservationRequestSerializer(serializers.Serializer):
    puesto = serializers.CharField(max_length=2)
    email = serializers.EmailField()