from  . import views
from django.urls import include, path
from rest_framework import routers
from django.db import router
from .views import puesto_Viewsets, reserva_Viewsets

router = routers.DefaultRouter()
router.register(r'puesto', puesto_Viewsets),
router.register(r'reserva', reserva_Viewsets),

urlpatterns = [
    path('', views.inicio, name='home' ),
    path('reserva/', views.reserva, name='reserva' ) ,
    path ('' , include(router.urls)), #Inclusion de las rutas del router
    path ('accounts/', include ('django.contrib.auth.urls')), #Herramientas de Django
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'), #Herramientas de Django
    path('obtener_sillas/', views.obtener_sillas),
    path ('putdelete_sillas/<int:pk>', views.putdelete_sillas),
    path('obtener_reservas/', views.obtener_reservas),
    path ('putdelete_reservas/<int:pk>', views.putdelete_reservas),
    path('eliminar_reserva/<id>',views.eliminar_reserva, name='eliminar_reserva'),
    path('puestos_disponibles/',views.puestos_disponibles),   
]







    


